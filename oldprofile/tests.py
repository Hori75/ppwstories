from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from selenium.webdriver.common.by import By


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class OldProfileTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get(reverse('oldprofile:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'oldprofile/index.html')

    def test_profile_url_status_200(self):
        response = self.client.get(reverse('oldprofile:profile'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'oldprofile/profile.html')


class OldProfileFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists_and_accesible(self):
        self.selenium.get(f'{self.live_server_url}/ppwstory1/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/ppwstory1/"]'))
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_profile_url_exists_and_accesible(self):
        self.selenium.get(f'{self.live_server_url}/ppwstory1/')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/ppwstory1/profile/"]'))
        
        self.selenium.get(f'{self.live_server_url}/ppwstory1/profile/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_mainblog_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/ppwstory1/')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/"]'))
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())