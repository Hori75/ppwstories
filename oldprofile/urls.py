from django.urls import include, path

from . import views

app_name = "oldprofile"

urlpatterns = [
    path('', views.index, name="index"),
    path('profile/', views.viewProfile, name="profile")
]
