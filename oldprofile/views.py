from django.shortcuts import render

def index(request):
    return render(request, 'oldprofile/index.html')

def viewProfile(request):
    return render(request, 'oldprofile/profile.html')
