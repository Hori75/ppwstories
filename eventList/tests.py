from .models import Event, EventForm, Participant, ParticipantForm
from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from selenium.webdriver.common.by import By


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class EventListTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get(reverse('eventList:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'eventList/index.html')

    def test_join_url_redirect_to_root(self):
        response = self.client.get(reverse('eventList:join', args=[99]), follow=True)
        self.assertRedirects(response, reverse('eventList:index'), status_code=302, target_status_code=200, fetch_redirect_response=True)

    def test_join_url_form_get(self):
        event = Event(title= "This is Title", organizer = "Mr. Organizer")
        event.save()
        response = self.client.get(reverse('eventList:join', args=[1]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'eventList/join.html')

    def test_join_url_form_post_no_data(self):
        event = Event(title= "This is Title", organizer = "Mr. Organizer")
        event.save()
        response = self.client.post(reverse('eventList:join', args=[1]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'eventList/join.html')

    def test_join_url_form_post_success(self):
        event = Event(title= "This is Title", organizer = "Mr. Organizer")
        event.save()
        response = self.client.post(reverse('eventList:join', args=[1]), data= {"name" : "Mr. Participant"})
        self.assertRedirects(response, reverse('eventList:index'), status_code=302, target_status_code=200, fetch_redirect_response=True)
        self.assertIsNotNone(Participant.objects.filter(pk=1))
        self.assertIn(Participant.objects.filter(pk=1).first(), event.participant_set.all())
        self.assertIn("Mr. Participant", str(event.participant_set.all()))

    def test_event_form_get(self):
        response = self.client.get(reverse("eventList:add"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'eventList/add.html')

    def test_event_form_post_no_data(self):
        response = self.client.post(reverse("eventList:add"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'eventList/add.html')

    def test_event_form_post_success(self):
        response = self.client.post(reverse("eventList:add"), data= {
                "title" : "This is Title",
                "organizer" : "Mr. Organizer"
                }
        )
        self.assertRedirects(response, reverse('eventList:index'), status_code=302, target_status_code=200, fetch_redirect_response=True)
        self.assertIsNotNone(Event.objects.filter(pk=1))
        self.assertIn("This is Title", str(Event.objects.all()))


class EventListFunctionalTestCase(FunctionalTestCase):
    def test_add_url_exists_and_usable(self):
        self.selenium.get(f'{self.live_server_url}/event/')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/event/add/"]'))

        self.selenium.get(f'{self.live_server_url}/event/add/')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//input[@name="title"]'))
        self.assertTrue(self.selenium.find_elements(By.XPATH, '//input[@name="organizer"]'))
        self.assertTrue(self.selenium.find_elements(By.XPATH, '//input[@type="submit"]'))

    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/event/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_event_exists_and_accesible(self):
        event = Event(title="This is Title", organizer="Mr. Organizer")
        event.save()
        event.participant_set.create(name="Mr. Participant1")
        event.participant_set.create(name="Mr. Participant2")

        self.selenium.get(f'{self.live_server_url}/event/')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertIn('This is Title', html.text)
        self.assertIn('Organizer: Mr. Organizer', html.text)
        self.assertIn('Mr. Participant1', html.text)
        self.assertIn('Mr. Participant2', html.text)

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/event/join/1"]'))

        self.selenium.get(f'{self.live_server_url}/event/join/1')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
        self.assertIn('This is Title', html.text)
        self.assertIn('Organizer: Mr. Organizer', html.text)
        self.assertIn('Mr. Participant1', html.text)
        self.assertIn('Mr. Participant2', html.text)

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//input[@name="name"]'))
        self.assertTrue(self.selenium.find_elements(By.XPATH, '//input[@type="submit"]'))

 