from .models import Event, EventForm, Participant, ParticipantForm
from django.contrib.messages import get_messages, success, error
from django.shortcuts import redirect, render, reverse

def index(request):
    messages = get_messages(request)
    events = Event.objects.all()
    return render(request, 'eventList/index.html', {'events' : events, 'messages' : messages})

def add(request):
    if (request.method == "POST"):
        form = EventForm(request.POST)
        if (form.is_valid()):
            event = EventForm.save(form)
            success(request, "Kegiatan " + event.title + " telah ditambahkan!")
            return redirect(reverse("eventList:index"))
        else:
            return render(request, 'eventList/add.html', {'form' : form})
    else:
        form = EventForm()
        return render(request, 'eventList/add.html', {'form' : form})

def join(request, id):
    event = Event.objects.filter(pk=id).first()
    if (event == None):
        error(request, "Kegiatan tidak ditemukan!")
        return redirect(reverse("eventList:index"))
    if (request.method == "POST"):
        form = ParticipantForm(request.POST)
        if (form.is_valid()):
            participant = ParticipantForm.save(form, commit=False)
            participant.event = event
            participant.save()
            success(request, "Anda telah bergabung ke kegiatan "+ event.title + "!")
            return redirect(reverse("eventList:index"))
        else:
            return render(request, 'eventList/join.html', {'form': form, 'event' : event})
    else:
        form = ParticipantForm()
        return render(request, 'eventList/join.html', {'form' : form, 'event' : event})
