from django.urls import path

from . import views

app_name = "eventList"

urlpatterns = [
    path("", views.index, name="index"),
    path("add/", views.add, name="add"),
    path("join/<int:id>", views.join, name="join")
]