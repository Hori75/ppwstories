from django.apps import AppConfig


class EventlistConfig(AppConfig):
    name = 'eventList'
