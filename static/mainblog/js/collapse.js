$(document).ready(function() {
    $(".collapsible").on("click", function() {
        this.classList.toggle("active");
        var content = document.getElementById(this.getAttribute("data-target").slice(1));
        if (content.style.maxHeight) {
            content.style.maxHeight = null;
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
        }
    });

    $('.down').on("click", function() {
        var own = this.parentElement.parentElement.parentElement.parentElement.parentElement;
        if (own.nextElementSibling)
            own.parentNode.insertBefore(own.nextElementSibling, own);
    });

    $('.up').on("click", function() {
        var own = this.parentElement.parentElement.parentElement.parentElement.parentElement;
        if (own.previousElementSibling)
            own.parentNode.insertBefore(own, own.previousElementSibling);
    });
});