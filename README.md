# PPW Stories

[![coverage report](https://gitlab.com/Hori75/ppwstories/badges/master/coverage.svg)](https://gitlab.com/Hori75/ppwstories/-/commits/master)
[![pipeline status](https://gitlab.com/Hori75/ppwstories/badges/master/pipeline.svg)](https://gitlab.com/Hori75/ppwstories/-/commits/master)

Journey into web development (with Django).  
Deployed on heroku.  
  
https://ppwstorieswilliam.herokuapp.com

