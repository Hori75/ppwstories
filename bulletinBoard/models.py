from django.contrib.auth.models import User
from django import forms
from django.db import models

class BulletinPost(models.Model):
    content = models.TextField(max_length=2000)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    postTime = models.DateTimeField(auto_now_add=True)

class BulletinPostForm(forms.ModelForm):
    class Meta:
        model = BulletinPost
        fields = ['content']

