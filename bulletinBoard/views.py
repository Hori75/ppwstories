import json

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseBadRequest, HttpResponseForbidden, JsonResponse
from django.shortcuts import redirect, render, reverse
from django.utils.html import escape

from .models import BulletinPost, BulletinPostForm

def index(request):
    form = BulletinPostForm()
    return render(request, 'bulletinBoard/index.html', {'form' : form})

def view(request):
    page = request.GET.get('p')
    if (page == None):
        return redirect(reverse('bulletinBoard:view')+"?p=1")
    page = int(page)
    bulletinpost = BulletinPost.objects.order_by("-postTime")
    if (len(bulletinpost) < (page-1)*10):
        page = len(bulletinpost) // 10 + 1
        return redirect(reverse('bulletinBoard:view')+"?p="+str(page))
    bulletinpost = bulletinpost[(page-1)*10:page*10]
    result = dict()
    i = 0
    for a in bulletinpost:
        post = {
            "content" : escape(a.content),
            "username" : a.user.username,
            "postTime" : str(a.postTime)
        }
        result[i] = post
        i += 1
    return JsonResponse(result)

def post(request):
    if (not request.user.is_authenticated):
        return HttpResponseForbidden()
    if (request.method == "GET"):
        return HttpResponseForbidden()
    form = BulletinPostForm(request.POST or None)
    if (not form.is_valid()):
        return HttpResponseBadRequest()
    else:
        post = form.save(commit=False)
        post.user = request.user
        post.save()
        return JsonResponse({'status' : 'success'})


def login_view(request):
    if (request.user.is_authenticated):
        return redirect(reverse('bulletinBoard:index'))
    elif (request.method == "POST"):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if (user is not None):
            login(request, user)
            return redirect(reverse('bulletinBoard:index'))
        else:
            return render(request, 'bulletinBoard/login.html', { "error" : "Your username or password is incorrect."})
    else: 
        return render(request, 'bulletinBoard/login.html')

def logout_view(request):
    logout(request)
    return redirect(reverse('bulletinBoard:index'))

def signUp_view(request):
    if (request.user.is_authenticated):
        return redirect(reverse('bulletinBoard:index'))
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect(reverse('bulletinBoard:index'))
    else:
        form = UserCreationForm()
    return render(request, 'bulletinBoard/signup.html', {'form': form})
