from django.urls import  path

from . import views

app_name = 'bulletinBoard'

urlpatterns = [
    path('', views.index, name="index"),
    path('post/', views.post, name="post"),
    path('view', views.view, name="view"),
    path('login/', views.login_view, name="login"),
    path('logout/', views.logout_view, name="logout"),
    path('signup/', views.signUp_view, name="signup")
]