from django.contrib.auth.models import User
from django.test import LiveServerTestCase, TestCase, tag
from django.test.client import RequestFactory
from django.urls import reverse
from selenium import webdriver

from .models import BulletinPost
from .views import login_view, signUp_view, post

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class BulletinBoardTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='Dummy', password='Blablabla123')

    def test_root_url_status_200(self):
        response = self.client.get(reverse('bulletinBoard:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "bulletinBoard/index.html")

    def test_view_url_no_query(self):
        response = self.client.get(reverse('bulletinBoard:view'))
        self.assertRedirects(response, reverse('bulletinBoard:view')+"?p=1")

    def test_view_url_empty_page_2(self):
        response = self.client.get(reverse('bulletinBoard:view')+"?p=2")
        self.assertRedirects(response, reverse('bulletinBoard:view')+"?p=1")

    def test_view_url_status_200(self):
        response  = self.client.get(reverse('bulletinBoard:view')+"?p=1")
        self.assertEqual(response.status_code, 200)

    def test_view_url_ordered(self):
        a = BulletinPost(content="This is me!", user=self.user)
        a.save()
        b = BulletinPost(content="This is the latter me!", user=self.user)
        b.save()
        response  = self.client.get(reverse('bulletinBoard:view')+"?p=1")
        self.assertEqual(response.status_code, 200)
        #self.assertJSONEqual(response, "{0 : {'content' : 'This is the latter me!', 'username' :" + self.user.username + ",'postTime' :" + str(b.postTime) + "},1 : {'content' : 'This is me!','username' : " + self.user.username + ",'postTime' :" + str(a.postTime) + "}}")

    def test_post_url_get_forbidden(self):
        request = self.factory.get(reverse('bulletinBoard:post'))
        request.user = self.user
        response  = post(request)
        self.assertEqual(response.status_code, 403)
    
    def test_post_url_post_not_authenticated(self):
        response = self.client.post(reverse('bulletinBoard:post'), {'content' : 'This is me!'})
        self.assertEqual(response.status_code, 403)

    def test_post_url_post_no_data(self):
        request = self.factory.post(reverse('bulletinBoard:post'))
        request.user = self.user
        response = post(request)
        self.assertEqual(response.status_code, 400)

    def test_post_url_post_status_200(self):
        request = self.factory.post(reverse('bulletinBoard:post'), {'content' : 'This is me!'})
        request.user = self.user
        response = post(request)
        self.assertEqual(response.status_code, 200)
        #self.assertJSONEqual(response, "{'status' : 'success'}")
        self.assertEqual(BulletinPost.objects.all().first().content, "This is me!")

    def test_login_url_status_200(self):
        response = self.client.get(reverse('bulletinBoard:login'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "bulletinBoard/login.html")
        self.assertContains(response, "Username")
        self.assertContains(response, "Password")
        self.assertContains(response, "Login")

    def test_login_post_no_data(self):
        response = self.client.post(reverse('bulletinBoard:login'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "bulletinBoard/login.html")

    def test_login_post_incorrect(self):
        response = self.client.post(reverse('bulletinBoard:login'), data={
            "username" : "Dummy",
            "password" : "Thisiswrong"
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "bulletinBoard/login.html")
        self.assertContains(response, "Your username or password is incorrect.")

    def test_login_post_success(self):
        response = self.client.post(reverse('bulletinBoard:login'), data={
            "username" : "Dummy",
            "password" : "Blablabla123"
        })
        self.assertRedirects(response, reverse("bulletinBoard:index"))

    def test_login_url_already_authenticated(self):
        request = self.client.get(reverse('bulletinBoard:login'))
        request.user = self.user
        response = login_view(request)
        self.assertEqual(response.status_code, 302)

    def test_signup_url_status_200(self):
        response = self.client.get(reverse('bulletinBoard:signup'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "bulletinBoard/signup.html")
        self.assertContains(response, "Username")
        self.assertContains(response, "Password")
        self.assertContains(response, "Password confirmation")

    def test_signup_post_no_data(self):
        response = self.client.post(reverse('bulletinBoard:signup'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "bulletinBoard/signup.html")

    def test_signup_post_same_username(self):
        response = self.client.post(reverse('bulletinBoard:signup'), data={
            "username" : "Dummy",
            "password1" : "ThisShouldbeGood1245",
            "password2" : "ThisShouldbeGood1245"
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "bulletinBoard/signup.html")
        #self.assertContains(response, "The username has already been taken")

    def test_signup_post_success(self):
        response = self.client.post(reverse('bulletinBoard:signup'), data={
            "username" : "Dummy2",
            "password1" : "ThisShouldbeGood1245",
            "password2" : "ThisShouldbeGood1245"
        })
        self.assertRedirects(response, reverse('bulletinBoard:index'))

    def test_signup_url_already_authenticated(self):
        request = self.client.get(reverse('bulletinBoard:signup'))
        request.user = self.user
        response = signUp_view(request)
        self.assertEqual(response.status_code, 302)


class BulletinBoardFunctionalTestCase(FunctionalTestCase):
    def test_login_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/bulletin/login/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_signup_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/bulletin/signup/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())