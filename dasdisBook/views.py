import requests
from django.shortcuts import render
from django.http import HttpResponseBadRequest, HttpRequest, JsonResponse

def index(request):
    return render(request, 'dasdisBook/index.html')

def search(request):
    query = request.GET.get('q')
    if (query == None or len(query) == 0):
        return HttpResponseBadRequest()
    else:
        googleResponse = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + query)
        return JsonResponse(googleResponse.json())