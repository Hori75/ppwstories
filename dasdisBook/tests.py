from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from selenium.webdriver.common.by import By


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainBlogTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get(reverse('dasdisBook:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'dasdisBook/index.html')
        self.assertContains(response, "Does this book exist?")

    def test_search_url_status_200(self):
        # Just test if it works or not
        response  = self.client.get(reverse('dasdisBook:search')+"?q=Django")
        self.assertEqual(response.status_code, 200)

    def test_search_url_no_query(self):
        # Let's just send 400 response
        response = self.client.get(reverse('dasdisBook:search')+'?q=')
        self.assertEqual(response.status_code, 400)

class MainBlogFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists_and_accesible(self):
        self.selenium.get(f'{self.live_server_url}/dasdisbook/')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())