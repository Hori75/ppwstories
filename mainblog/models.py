from django.db import models

class Post(models.Model):
    title = models.CharField(max_length=100)
    mini_thumbnail = models.CharField(max_length=100)
    thumbnail = models.CharField(max_length=100)
    summary = models.TextField(max_length=250)
    content = models.TextField(max_length=4000)

    def __str__(self):
        return self.title
