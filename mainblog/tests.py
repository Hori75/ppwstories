from .models import Post
from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from selenium.webdriver.common.by import By


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainBlogTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get(reverse('mainblog:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mainblog/index.html')

    def test_profile_url_status_200(self):
        response = self.client.get(reverse('mainblog:profile'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mainblog/profile.html')

    def test_posts_url_redirect_to_root(self):
        response = self.client.get(reverse('mainblog:posts'))
        self.assertRedirects(response, reverse('mainblog:index'), status_code=302, target_status_code=200, fetch_redirect_response=True)

    def test_post_url_status_200(self):
        post = Post(
            title= "This is Title", 
            mini_thumbnail = "mainblog/img/testminithumbnail.png",
            thumbnail = "mainblog/img/testthumbnail.jpg",
            summary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum eu ipsum eu sodales. Duis vestibulum dolor id ante sodales.",
        )
        post.save()
        response = self.client.get(reverse('mainblog:posts', args=[1]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mainblog/post.html')
        self.assertIn("This is Title", str(Post.objects.all()))


class MainBlogFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists_and_accesible(self):
        self.selenium.get(f'{self.live_server_url}')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/profile/"]'))
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_profile_url_exists_and_accesible(self):
        self.selenium.get(f'{self.live_server_url}')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/profile/"]'))
        self.selenium.get(f'{self.live_server_url}/profile/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_matkulList_url_accesible(self):
        self.selenium.get(f'{self.live_server_url}')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/matkul/"]'))
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_eventList_url_accesible(self):
        self.selenium.get(f'{self.live_server_url}')
        html = self.selenium.find_element_by_tag_name('html')
        
        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/event/"]'))
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_oldprofile_url_accesible(self):
        self.selenium.get(f'{self.live_server_url}')
        html = self.selenium.find_element_by_tag_name('html')
        
        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/ppwstory1/"]'))
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_dasdisbook_url_accesible(self):
        self.selenium.get(f'{self.live_server_url}')
        html = self.selenium.find_element_by_tag_name('html')
        
        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/dasdisbook/"]'))
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_post_url_exists_and_accesible(self):
        post = Post(
            title= "This is Title", 
            mini_thumbnail = "mainblog/img/testminithumbnail.png",
            thumbnail = "mainblog/img/testthumbnail.jpg",
            summary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum eu ipsum eu sodales. Duis vestibulum dolor id ante sodales.",
        )
        post.save()
        self.selenium.get(f'{self.live_server_url}')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertIn('This is Title', html.text)
        self.assertIn('Lorem ipsum dolor sit amet, consectetur adipiscing elit.', html.text)

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/posts/1"]'))

        self.selenium.get(f'{self.live_server_url}/posts/1')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
        self.assertIn('This is Title', html.text)
        self.assertIn('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum eu ipsum eu sodales. Duis vestibulum dolor id ante sodales.', html.text)