from django.http import Http404
from django.shortcuts import redirect, render, reverse, get_object_or_404
from django.template import TemplateDoesNotExist

from .models import Post

def index(request):
    allPosts = Post.objects.all()
    return render(request, 'mainblog/index.html', {'allPosts': allPosts})

def profile(request):
    return render(request, 'mainblog/profile.html')

def posts(request, number=0):
    if (number==0):
        return redirect(reverse('mainblog:index'))
    else:
        post = get_object_or_404(Post, pk=number)
        return render(request, 'mainblog/post.html', {'post' : post})
