from .models import MatKul, MatKulForm
from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from selenium.webdriver.common.by import By


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MatkulListTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get(reverse('matkulList:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'matkulList/index.html')

    def test_view_url_redirect_to_root(self):
        response = self.client.get(reverse('matkulList:view', args=[99]), follow=True)
        self.assertRedirects(response, reverse('matkulList:index'), status_code=302, target_status_code=200, fetch_redirect_response=True)

    def test_view_url_status_200(self):
        matkul = MatKul(
            title= "This is Title",
            term = "Gasal 2019/2020", 
            lecturer = "Dr. Lecturer",
            total_credits = 3,
            location = "Online",
            description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum eu ipsum eu sodales. Duis vestibulum dolor id ante sodales.",
        )
        matkul.save()
        response = self.client.get(reverse('matkulList:view', args=[1]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'matkulList/view.html')

    def test_maktul_form_get(self):
        response = self.client.get(reverse("matkulList:add"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'matkulList/add.html')

    def test_matkul_form_post_no_data(self):
        response = self.client.post(reverse("matkulList:add"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'matkulList/add.html')

    def test_matkul_form_post_error(self):
        response = self.client.post(reverse("matkulList:add"), data= {
                "title" : "This is Title",
                "term"  : "Gasal 2019/20", 
                "lecturer" : "Dr. Lecturer",
                "total_credits" : 3,
                "location" : "Online",
                "description" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum eu ipsum eu sodales. Duis vestibulum dolor id ante sodales.",
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'matkulList/add.html')

    def test_matkul_form_post_success(self):
        response = self.client.post(reverse("matkulList:add"), data= {
                "title" : "This is Title",
                "term"  : "Gasal 2019/2020", 
                "lecturer" : "Dr. Lecturer",
                "total_credits" : 3,
                "location" : "Online",
                "description" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum eu ipsum eu sodales. Duis vestibulum dolor id ante sodales.",
            }
        )
        self.assertRedirects(response, reverse('matkulList:index'), status_code=302, target_status_code=200, fetch_redirect_response=True)
        self.assertIsNotNone(MatKul.objects.filter(pk=1))
        self.assertIn("This is Title", str(MatKul.objects.all()))

    def test_delete_url_redirect_to_root(self):
        response = self.client.get(reverse('matkulList:delete', args=[99]), follow=True)
        self.assertRedirects(response, reverse('matkulList:index'), status_code=302, target_status_code=200, fetch_redirect_response=True)

    def test_delete_url_get_success(self):
        matkul = MatKul(
            title= "This is Title",
            term = "Gasal 2019/2020", 
            lecturer = "Dr. Lecturer",
            total_credits = 3,
            location = "Online",
            description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum eu ipsum eu sodales. Duis vestibulum dolor id ante sodales.",
        )
        matkul.save()
        response = self.client.get(reverse('matkulList:delete', args=[1]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'matkulList/remove.html')
    
    def test_delete_url_post_success(self):
        matkul = MatKul(
            title= "This is Title",
            term = "Gasal 2019/2020", 
            lecturer = "Dr. Lecturer",
            total_credits = 3,
            location = "Online",
            description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum eu ipsum eu sodales. Duis vestibulum dolor id ante sodales.",
        )
        matkul.save()
        response = self.client.post(reverse('matkulList:delete', args=[1]))
        self.assertRedirects(response, reverse('matkulList:index'), status_code=302, target_status_code=200, fetch_redirect_response=True)
        self.assertIsNone(MatKul.objects.filter(pk=1).first())
        self.assertNotIn("This is Title", str(MatKul.objects.all()))



class MatkulListFunctionalTestCase(FunctionalTestCase):
    def test_add_url_exists_and_usable(self):
        self.selenium.get(f'{self.live_server_url}/matkul/')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/matkul/add/"]'))

        self.selenium.get(f'{self.live_server_url}/matkul/add/')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//input[@name="title"]'))
        self.assertTrue(self.selenium.find_elements(By.XPATH, '//input[@name="term"]'))
        self.assertTrue(self.selenium.find_elements(By.XPATH, '//input[@name="lecturer"]'))
        self.assertTrue(self.selenium.find_elements(By.XPATH, '//input[@name="total_credits"]'))
        self.assertTrue(self.selenium.find_elements(By.XPATH, '//input[@name="location"]'))
        self.assertTrue(self.selenium.find_elements(By.XPATH, '//textarea[@name="description"]'))
        self.assertTrue(self.selenium.find_elements(By.XPATH, '//input[@type="submit"]'))

    def test_delete_url_exists_and_usable(self):
        matkul = MatKul(
            title= "This is Title",
            term = "Gasal 2019/2020", 
            lecturer = "Dr. Lecturer",
            total_credits = 3,
            location = "Online",
            description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum eu ipsum eu sodales. Duis vestibulum dolor id ante sodales.",
        )
        matkul.save()

        self.selenium.get(f'{self.live_server_url}/matkul/view/1')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/matkul/delete/1"]'))

        self.selenium.get(f'{self.live_server_url}/matkul/delete/1')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//form[@method="POST"]//input[@value="Delete"]'))

    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/matkul/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_view_url_exists_and_accesible(self):
        matkul = MatKul(
            title= "This is Title",
            term = "Gasal 2019/2020", 
            lecturer = "Dr. Lecturer",
            total_credits = 3,
            location = "Online",
            description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum eu ipsum eu sodales. Duis vestibulum dolor id ante sodales.",
        )
        matkul.save()

        self.selenium.get(f'{self.live_server_url}/matkul/')
        html = self.selenium.find_element_by_tag_name('html')

        self.assertIn('This is Title', html.text)
        self.assertIn('Gasal 2019/2020', html.text)
        self.assertIn('Lecturer: Dr. Lecturer', html.text)
        self.assertIn('Total credits: 3', html.text)
        self.assertIn('Location: Online', html.text)

        self.assertTrue(self.selenium.find_elements(By.XPATH, '//a[@href="/matkul/view/2"]'))

        self.selenium.get(f'{self.live_server_url}/matkul/view/2')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
        self.assertIn('This is Title', html.text)
        self.assertIn('Gasal 2019/2020', html.text)
        self.assertIn('Lecturer: Dr. Lecturer', html.text)
        self.assertIn('Total credits: 3', html.text)
        self.assertIn('Location: Online', html.text)
        self.assertIn('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum eu ipsum eu sodales. Duis vestibulum dolor id ante sodales.', html.text)

    
